﻿namespace GeViScopeLibraryTest
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonCameraSelect = new System.Windows.Forms.Button();
            this.BtnTakeSnapshot = new System.Windows.Forms.Button();
            this.BtnSaveSnapshot = new System.Windows.Forms.Button();
            this.BtnCancelSnapshot = new System.Windows.Forms.Button();
            this.BtnExitSnapshot = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonCameraSelect
            // 
            this.ButtonCameraSelect.Location = new System.Drawing.Point(12, 12);
            this.ButtonCameraSelect.Name = "ButtonCameraSelect";
            this.ButtonCameraSelect.Size = new System.Drawing.Size(286, 23);
            this.ButtonCameraSelect.TabIndex = 0;
            this.ButtonCameraSelect.Text = "Snapshot Form";
            this.ButtonCameraSelect.UseVisualStyleBackColor = true;
            this.ButtonCameraSelect.Click += new System.EventHandler(this.ButtonCameraSelect_Click);
            // 
            // BtnTakeSnapshot
            // 
            this.BtnTakeSnapshot.Location = new System.Drawing.Point(12, 41);
            this.BtnTakeSnapshot.Name = "BtnTakeSnapshot";
            this.BtnTakeSnapshot.Size = new System.Drawing.Size(286, 23);
            this.BtnTakeSnapshot.TabIndex = 2;
            this.BtnTakeSnapshot.Text = "Take Snapshot";
            this.BtnTakeSnapshot.UseVisualStyleBackColor = true;
            this.BtnTakeSnapshot.Click += new System.EventHandler(this.BtnTakeSnapshot_Click);
            // 
            // BtnSaveSnapshot
            // 
            this.BtnSaveSnapshot.Location = new System.Drawing.Point(12, 70);
            this.BtnSaveSnapshot.Name = "BtnSaveSnapshot";
            this.BtnSaveSnapshot.Size = new System.Drawing.Size(286, 23);
            this.BtnSaveSnapshot.TabIndex = 3;
            this.BtnSaveSnapshot.Text = "Save Snapshot";
            this.BtnSaveSnapshot.UseVisualStyleBackColor = true;
            this.BtnSaveSnapshot.Click += new System.EventHandler(this.BtnSaveSnapshot_Click);
            // 
            // BtnCancelSnapshot
            // 
            this.BtnCancelSnapshot.Location = new System.Drawing.Point(12, 99);
            this.BtnCancelSnapshot.Name = "BtnCancelSnapshot";
            this.BtnCancelSnapshot.Size = new System.Drawing.Size(286, 23);
            this.BtnCancelSnapshot.TabIndex = 4;
            this.BtnCancelSnapshot.Text = "Cancel Snapshot";
            this.BtnCancelSnapshot.UseVisualStyleBackColor = true;
            this.BtnCancelSnapshot.Click += new System.EventHandler(this.BtnCancelSnapshot_Click);
            // 
            // BtnExitSnapshot
            // 
            this.BtnExitSnapshot.Location = new System.Drawing.Point(12, 128);
            this.BtnExitSnapshot.Name = "BtnExitSnapshot";
            this.BtnExitSnapshot.Size = new System.Drawing.Size(286, 23);
            this.BtnExitSnapshot.TabIndex = 5;
            this.BtnExitSnapshot.Text = "Exit Snapshot";
            this.BtnExitSnapshot.UseVisualStyleBackColor = true;
            this.BtnExitSnapshot.Click += new System.EventHandler(this.BtnExitSnapshot_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 162);
            this.Controls.Add(this.BtnExitSnapshot);
            this.Controls.Add(this.BtnCancelSnapshot);
            this.Controls.Add(this.BtnSaveSnapshot);
            this.Controls.Add(this.BtnTakeSnapshot);
            this.Controls.Add(this.ButtonCameraSelect);
            this.Name = "MainForm";
            this.Text = "Snapshot Library Test";
            this.Click += new System.EventHandler(this.MainForm_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonCameraSelect;
        private System.Windows.Forms.Button BtnTakeSnapshot;
        private System.Windows.Forms.Button BtnSaveSnapshot;
        private System.Windows.Forms.Button BtnCancelSnapshot;
        private System.Windows.Forms.Button BtnExitSnapshot;
    }
}

