﻿using GeViScopeLibrary;
using System;
using System.Windows.Forms;

namespace GeViScopeLibraryTest
{
    public partial class MainForm : Form
    {
        private GeViScopeForm snapshotForm;

        public MainForm()
        {
            InitializeComponent();
            snapshotForm = new GeViScopeForm(Handle);
        }

        private void ButtonCameraSelect_Click(object sender, EventArgs e)
        {
            snapshotForm.Show();
            snapshotForm.WindowState = FormWindowState.Normal;
            snapshotForm.BringToFront();
        }

        private void BtnTakeSnapshot_Click(object sender, EventArgs e)
        {
            snapshotForm.ShowMessage("VERIFY SNAPSHOT...");
            snapshotForm.TakeSnapshot("This is a comment");
        }

        private void BtnSaveSnapshot_Click(object sender, EventArgs e)
        {
            String snapShotFile = "C:" + @"\\Users" + @"\Public" + @"\Documents" + @"\GeViScopeLibraryTest" + "-" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + "-" + DateTime.Now.Second.ToString() + ".bmp";
            //snapshotForm.SaveSnapshot("C://Users/Public/Documents/test.bmp");
            //snapshotForm.SaveSnapshot(@"C:\\Users\Public\Documents\test.bmp");
            snapshotForm.SaveSnapshot(snapShotFile);
        }

        private void BtnCancelSnapshot_Click(object sender, EventArgs e)
        {
            snapshotForm.ShowVideo();
            snapshotForm.CancelSnapshot();
        }

        private void BtnExitSnapshot_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show(this, "EXIT SNAPSHOT TEST", "SNAPSHOT TEST", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                snapshotForm.Close();

                // Exit the application (close windows down)
                Application.Exit();
            }
        }

        private void MainForm_Click(object sender, EventArgs e)
        {
            snapshotForm.Close();

            // Exit the application (close windows down)
            Application.Exit();
        }
    }
}
