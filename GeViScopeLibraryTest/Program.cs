﻿using System;
using System.Windows.Forms;

namespace GeViScopeLibraryTest
{
    static class Program
    {
        private static MainForm mainForm = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            mainForm = new MainForm();
            Application.Run(mainForm);

            // Terminate the process
            Environment.Exit(0);
        }
    }
}
